# Netlify cms starter kit

config.yml in admin folder

### local backend

if 
```
local_backend: true # for local update
```

then 

```
run `npx netlify-cms-proxy-server` for local server
```

is available

### authorization through

chaussette is a project!

```
backend:
  name: gitlab
  branch: main
  repo: group/reponame
  auth_type: implicit # Required for implicit grant
  app_id: app id given by gitlab # Application ID from your GitLab settings
  api_root: https://gitlab.chaussette.com/api/v4
  base_url: https://gitlab.chaussette.com
  auth_endpoint: oauth/authorize
  site_domain: https://chaussette.com
```
